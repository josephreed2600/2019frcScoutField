function getBitFrom(index, number) {
	return (number >> index) % 2;
}

function setBitMark(index, number) {
	return number |= 1 << index;
}

function setBitClear(index, number) {
	return number &= ~(1 << index);
}

var evt0 = {
	type:  "score",  //  0
	ship:  "rocket", //  0
	side:  "left",   //  0
	horiz: "port",   // 00
	vert:  "fore",   // 00
	item:  "cargo"   //  0
}

var evt1 = {
	type:  "score",  //  0
	ship:  "rocket", //  0
	side:  "right",  //  1
	horiz: "center", // 01
	vert:  "mid",    // 01
	item:  "panel"   //  1
}

var evt2 = {
	type:  "score",     //  0
	ship:  "rocket",    //  0
	side:  "left",      //  0
	horiz: "starboard", // 10
	vert:  "aft",       // 10
	item:  "cargo"      //  0
}

var evt3 = {
	type:  "score",     //  0
	ship:  "cargo",     //  1
//	side:  "",          //  0
	horiz: "starboard", // 10
	vert:  "bow",       // 11
	item:  "panel"      //  1
}

function jsonToBin(event) {
	var bin = 0;

	var setType = function(e, n) {
		switch(e.type) {
			case "score": return setBitClear(0,n);
			default: throw "Heckin' event type is wrong, smh\n" + JSON.stringify(e);
		}
		// this point should probably never be reached
		return n;
	};
	var setShip = function(e, n) {
		switch(e.ship) {
			case "rocket": return setBitClear(1,n);
			case "cargo": return setBitMark(1,n);
			default: throw "Invalid ship type\n" + JSON.stringify(e);
		}
		// don't happen
		return n;
	};
	var setSide = function(e, n) {
		switch(e.side) {
			case "left": return setBitClear(2,n);
			case "right": return setBitMark(2,n);
			default: throw "Invalid side\n" + JSON.stringify(e);
		}
		// no
		return n;
	};
	var setHoriz = function(e, n) {
		switch(e.horiz) {
			case "port": return setBitClear(3,setBitClear(4,n));
			case "center": return setBitMark(3,setBitClear(4,n));
			case "starboard": return setBitClear(3,setBitMark(4,n));
			default: throw "Invalid horizontal position\n" + JSON.stringify(e);
		}
		// aaaaa
		return n;
	};
	var setVert = function(e, n) {
		switch(e.vert) {
			case "fore": return setBitClear(5,setBitClear(6,n));
			case "mid": return setBitMark(5,setBitClear(6,n));
			case "aft": return setBitClear(5,setBitMark(6,n));
			case "bow": return setBitMark(5,setBitMark(6,n));
			default: throw "Invalid vertical position (along the hot dog)\n" + JSON.stringify(e);
		}
		// i mean, if we don't throw something, we may never know
		return n;
	};
	var setItem = function(e, n) {
		switch(e.item) {
			case "cargo": return setBitClear(7,n);
			case "panel": return setBitMark(7,n);
			default: throw "Invalid game piece\n" + JSON.stringify(e);
		}
		// There is a small mailbox here.
		return n;
	};
	var setBits = function(e, n) {
		n = setType(e, n);
		switch(e.type) {
			case "score":
				n = setShip(e, n);
				if(e.ship == "rocket") n = setSide(e, n);
				n = setHoriz(e, n);
				n = setVert(e, n);
				n = setItem(e, n);
				break;
			default: break;
		}
		return n;
	};

	// let's do some ugly data validation
	if(event.ship == "cargo" && event.horiz == "center") throw "Yo, the uh, cargo ship doesn't have a center column.\n" + JSON.stringify(event);
	if(event.ship == "rocket" && event.vert == "bow") throw "Rocket doesn't hold stuff in the bow.\n" + JSON.stringify(event);

	return setBits(event, bin);
	// if something throws an error, everything will probably crash. just letting you know.
}

function binToJson(bin) {
	var event = {};
	event.type = getBitFrom(0,bin) ? "unknown" : "score";
	switch(event.type) {
		case "score":
			event.ship = getBitFrom(1,bin) ? "cargo" : "rocket";
			if(event.ship == "rocket") event.side = getBitFrom(2,bin) ? "right" : "left";
			var horiz = getBitFrom(3,bin) + 2*getBitFrom(4,bin);
			switch(horiz) {
				case 0: event.horiz = "port"; break;
				case 1: event.horiz = "center"; break;
				case 2: event.horiz = "starboard"; break;
				default: throw "Invalid horizontal position\nHoriz code: " + horiz + "\nBin: " + bin;
			}
			var vert = getBitFrom(5,bin) + 2*getBitFrom(6,bin);
			switch(vert) {
				case 0: event.vert = "fore"; break;
				case 1: event.vert = "mid"; break;
				case 2: event.vert = "aft"; break;
				case 3: event.vert = "bow"; break;
				default: throw "Invalid horizontal position\nHoriz code: " + horiz + "\nBin: " + bin;
			}
			event.item = getBitFrom(7,bin) ? "panel" : "cargo";
			break;
		default: break;
	}
	return event;
}

// Test whether two objects have the same properties with the same values, sometimes.
// http://adripofjavascript.com/blog/drips/object-equality-in-javascript.html
function objEq(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}

function test(event) {return objEq(event, binToJson(jsonToBin(event)));}
